/*sentencia switch

switch(expresion){
    case literal 1:
        conjunto de instrucciones 1;
        break;
    case literal 2:
        conjunto de instrucciones 2;
        break;
    case literal n:
        conjunto de instrucciones n;
        break;
    default:
        conjunto de instrucciones por defecto;
        break;
}*/
#include <iostream>
using namespace std;
int main()
{
    /*int num;
    cout << "digite un numero entre 1 - 5: ";
    cin>>num;
    switch(num){
        case 1: cout<<"el numero es 1"; break;
        case 2: cout<<"el numero es 2"; break;
        case 3: cout<<"el numero es 3"; break;
        case 4: cout<<"el numero es 4"; break;
        case 5: cout<<"el numero es 5"; break;
        default: cout<<"no esta en el rango de 1 - 5"; break;
    }*/
    //desarrolle un menu
// menu de operaciones
//1 suma
//2 resta
//3 multiplicacion
//4 division
//controlar la division entre 0 o un numero negativo
  /*  float n1,n2;
    int opc;
    cout<<"digite 2 numeros: ";
    cin>>n1>>n2;
    cout<<"----menu de operaciones----"<<endl;
    cout<<"1 suma"<<endl;
    cout<<"2 resta"<<endl;
    cout<<"3 multiplicacion"<<endl;
    cout<<"4 division"<<endl;
    cout<<"seleccione una opcion: ";
    cin>>opc;
    switch(opc){
        case 1: cout<<"la suma es: "<<n1+n2;break;
        case 2: cout<<"la resta es: "<<n1-n2;break;
        case 3: cout<<"la multiplicaion es: "<<n1*n2;break;
        case 4:
            if(n2 <= 0){
            cout<<"No se puede dividir";}
            else{cout<<"La division es: "<<n1/n2;};break;
        default: cout<<"no existe la opcion ingresada"; break;
    }*/
//escriba un programa que lea un caracter  indique si es una vocal minuscula o no
    char letra;
    cout<<"digite un caracter: ";
    cin>>letra;
    switch(letra){
        case 'a':
        case 'e':
        case 'i': cout<<"si"; break;
        case 'o':
        case 'u': cout<<"es minuscula"; break;
        default: cout<<"no es minuscula"; break;
    }
    return 0;
}
